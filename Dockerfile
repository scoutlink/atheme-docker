FROM alpine:latest as builder

ARG VERSION=v7.2.11
ARG SRC_URL=https://github.com/atheme/atheme/releases/download/${VERSION}/atheme-services-${VERSION}.tar.xz
ARG UID=1501

RUN apk add --no-cache g++ pkgconfig openssl-dev openldap-dev make git patch

RUN addgroup -g ${UID} -S atheme
RUN adduser -u ${UID} -h /atheme/ -D -S -G atheme atheme

RUN wget -q ${SRC_URL} && tar -xf atheme-services-${VERSION}.tar* && rm atheme-services-${VERSION}.tar* && mv atheme-services-${VERSION} /atheme-src


WORKDIR /atheme-src
COPY patches patches
RUN for PATCH in $(ls patches); do patch -p0 < patches/${PATCH}; done
RUN ./configure --prefix /atheme --disable-nls
RUN make && make install


# Stage 1: Create optimized runtime container
FROM alpine:3.9

ARG UID=1501

RUN apk add --no-cache openssl openldap ca-certificates msmtp && \
    addgroup -g ${UID} -S atheme && \
    adduser -u ${UID} -h /atheme/ -D -S -G atheme atheme

COPY --chown=atheme:atheme entrypoint.sh /entrypoint.sh
COPY --from=builder --chown=atheme:atheme /atheme/ /atheme/

USER atheme

WORKDIR /
ENTRYPOINT ["/entrypoint.sh"]

