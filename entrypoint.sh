#!/bin/sh

touch /atheme/.msmtprc
chmod 0600 /atheme/.msmtprc

cat << MSMTP_CONFIG > /atheme/.msmtprc
defaults
auth           on
tls            on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile        ~/.msmtp.log

account        default
host           ${SMTP_HOST}
port           ${SMTP_PORT}
user           ${SMTP_USER}
password       ${SMTP_PASS}

MSMTP_CONFIG


/atheme/bin/atheme-services -n
